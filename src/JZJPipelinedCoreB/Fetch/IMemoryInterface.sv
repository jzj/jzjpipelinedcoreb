module IMemoryInterface
#(
	parameter logic [31:0] RESET_VECTOR = 32'h00000000//Address for execution to begin at (must be within RAM)
)
(
	input logic clock,
	input logic reset,
	
	//Stage Control
	input logic stall_fetch,
	input logic stall_decode,
	input logic clear_decode,
	
	//Stage Outputs
	output reg [31:0] instruction_decode = 32'h00000013,//instruction at address pc_fetch
	
	//Intra-Stage Inputs
	input logic [31:0] newPC,
	
	//Memory Control Outputs
	output logic en_instructionAddress,//Whether to latch instructionAddress into instruction address
	
	//Memory Input
	input logic [31:0] instructionIn,//Instruction in from memory combinationally (big endian already)
	
	//Memory Output
	output logic [31:0] instructionAddress//Instruction address to latch when en_address is asserted
);

//The memory address register must be the same as the PC at all times, so...
assign instructionAddress = newPC;
assign en_instructionAddress = ~stall_fetch;//only fetch the new instruction (by latching new address) if fetch is not stalled (same as PCController logic)

//Stage output logic (for instruction_fetch)
always_ff @(posedge clock, posedge reset)
begin
	if (reset)
		instruction_decode <= 32'h00000013;//NOP
	else if (clock)
	begin
		if (clear_decode)
			instruction_decode <= 32'h00000013;//NOP
		else if (~stall_decode)
			instruction_decode <= instructionIn;//big endian already
	end
end

endmodule